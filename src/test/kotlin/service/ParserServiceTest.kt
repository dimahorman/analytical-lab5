package service

import config.JacksonConfig
import org.junit.Test
import kotlin.test.assertEquals

class ParserServiceTest {
    private val url = "https://data.city.kharkov.ua"
    private val path = "/sotsialnij-zakhist.html"
    private val mapper = JacksonConfig.defaultObjectMapper()
    private val parserService = ParserService(url, path, mapper)

    companion object {
        private const val expectedSize = 621
    }

    @Test
    fun parseTest() {
       val parsedValues = parserService.parse()
        assertEquals(expectedSize, parsedValues.size)
        println("done")
    }
}
