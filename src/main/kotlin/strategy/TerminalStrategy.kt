package strategy

import service.ParserService

class TerminalStrategy(
    private val parserService: ParserService,
) : OutputStrategy {

    override fun execute(parsedValues: List<String>) {
        printElements(parsedValues)
        printElements(parserService.getLinesQuantity(parsedValues.size))
    }

    private fun printElements(elements: List<String>) {
        elements.forEach { element ->
            println(element)
        }
    }
}
