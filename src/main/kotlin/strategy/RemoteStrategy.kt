package strategy

import service.EventHubService

class RemoteStrategy(
    private val eventHubService: EventHubService
) : OutputStrategy {

    override fun execute(parsedValues: List<String>) {
        sendEventsToHub(parsedValues)
    }

    private fun sendEventsToHub(events: List<String>) {
        events.forEach { event ->
            eventHubService.addEventToBatch(event)
        }
        eventHubService.sendData()
    }
}
