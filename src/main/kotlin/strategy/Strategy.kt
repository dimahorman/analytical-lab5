package strategy

import service.LogService
import service.ParserService
import java.time.LocalDateTime

interface OutputStrategy {
    fun execute(parsedValues: List<String>)
}

enum class StrategyOptions {
    TERMINAL,
    REMOTE
}

enum class ParsingStatus {
    WAITING,
    IN_PROGRESS,
    COMPLETED,
    FAILED
}

class Context(private val strategy: OutputStrategy,
              private val parserService: ParserService,
              private val logService: LogService,
              private val fileName: String) {

    private var executionStatus = ParsingStatus.WAITING

    fun execute() {
        val result = logService.getStatus(fileName)
        if (result == null) {
            executionStatus = ParsingStatus.IN_PROGRESS
            logService.setStatus(fileName, "status: ${executionStatus.name}, time: ${LocalDateTime.now()}")

            val parsedValues = parserService.parse()
            val linesQuantity = parserService.getLinesQuantity(parsedValues.size)

            strategy.execute(parsedValues)

            executionStatus = ParsingStatus.COMPLETED
            logService.setStatus(fileName, "status: ${executionStatus.name}, time: ${LocalDateTime.now()}")
            logService.pushLinesParsedLog(linesQuantity, fileName)
        } else {
            println("Repeated request to parse file $fileName")
            logService.pushLog(
                "$fileName-logger",
                "Repeated parsing request of $fileName. Time: ${LocalDateTime.now()}"
            )
            println(result)
        }
        println("log file: of $fileName parsing attempts")
        logService.printLogs("$fileName-logger")
    }
}
