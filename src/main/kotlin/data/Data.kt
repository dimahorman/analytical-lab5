package data

data class Article(
    val title: String,
    val lineNumber: Int,
    val wordQuantity: Int,
    val format: DocType,
    val date: String
)

enum class DocType {
    DOCX,
    XML,
    PDF,
    XLSX
}
