package config

import util.Secrets
import java.lang.RuntimeException

class UrlConfig {
    companion object {
        fun getUrl(): String {
            return Secrets.get("URL_VALUE") ?: throw RuntimeException("URL_VALUE was not assigned")
        }

        fun getPath(): String {
            return Secrets.get("URL_PATH") ?: throw RuntimeException("URL_PATH was not assigned")
        }
    }
}
