package config

import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisShardInfo
import util.Secrets

class RedisConfig {
    companion object {
        fun getRedisClient(useSsl: Boolean = true): Jedis {
            return Jedis(getShardInfo(useSsl))
        }

        private fun getShardInfo(useSsl: Boolean = true): JedisShardInfo {
            val redisHost = Secrets.get("REDIS_HOST") ?: throw RuntimeException("REDIS_HOST variable was not assigned")
            val redisPort = Secrets.get("REDIS_PORT") ?: throw RuntimeException("REDIS_PORT variable was not assigned")
            val key = Secrets.get("REDIS_KEY") ?: throw RuntimeException("REDIS_KEY variable was not assigned")
            val shardInfo = JedisShardInfo(redisHost, redisPort.toInt(), useSsl)
            shardInfo.password = key
            return shardInfo
        }
    }
}
