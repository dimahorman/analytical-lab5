package config

import com.azure.messaging.eventhubs.EventHubClientBuilder
import com.azure.messaging.eventhubs.EventHubProducerClient
import util.Secrets

class EventHubProducerConfig {
    companion object {
        fun getProducer(): EventHubProducerClient {

            val connectionString = Secrets.get("AZURE_EVENT_HUB_CONNECTION")
                ?: throw RuntimeException("AZURE_EVENT_HUB_CONNECTION was not assigned")

            val eventHubName =
                Secrets.get("AZURE_EVENT_HUB_NAME") ?: throw RuntimeException("AZURE_EVENT_HUB_NAME was not assigned")

            return EventHubClientBuilder()
                .connectionString(connectionString, eventHubName)
                .buildProducerClient();
        }
    }
}
