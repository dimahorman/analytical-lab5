import config.EventHubProducerConfig
import config.JacksonConfig
import config.RedisConfig
import config.UrlConfig
import service.EventHubService
import service.LogService
import service.ParserService
import strategy.Context
import strategy.RemoteStrategy
import strategy.StrategyOptions
import strategy.TerminalStrategy

fun main(args: Array<String>) {
    val url = UrlConfig.getUrl()
    val path = UrlConfig.getPath()

    val redisClient = RedisConfig.getRedisClient()
    val mapper = JacksonConfig.defaultObjectMapper()
    val parserService = ParserService(url, path, mapper)
    val eventHubService = EventHubService(EventHubProducerConfig.getProducer())
    val logService = LogService(redisClient)

    println("Response from Redis: ${redisClient.ping()}")
    println("Choose your strategy(remote or terminal):")
    val consoleInput = readLine()

    val context = if (consoleInput.equals(StrategyOptions.REMOTE.name, ignoreCase = true)) {
        println("Remote strategy is chosen")
        Context(RemoteStrategy(eventHubService), parserService, logService, path)
    } else {
        println("Terminal strategy is chosen")
        Context(TerminalStrategy(parserService), parserService, logService, path)
    }
    context.execute()
}
