package service

import com.azure.messaging.eventhubs.EventData
import com.azure.messaging.eventhubs.EventDataBatch
import com.azure.messaging.eventhubs.EventHubProducerClient
import java.math.BigInteger

class EventHubService(private val producer: EventHubProducerClient) {
    private lateinit var batch: EventDataBatch

    init {
        createNewBatch()
    }

    private fun createNewBatch() {
        batch = producer.createBatch()
    }

    fun addEventToBatch(event: String) {
        println("Initiating data adding to batch... ")
        val isAdded = batch.tryAdd(EventData(event))
        if (!isAdded) {
            println("Not enough space in batch... sending previous data")
            sendData()
            println("Trying to push the data to a new batch...")
            addEventToBatch(event)
        } else {
            println("Event has been added to batch")
        }
    }

    fun sendData() {
        println("Initiating data sending to event hub... ")
        if (isBatchEmpty()) {
            println("Batch is empty, nothing here to send... ")
        } else {
            producer.send(batch)
            println("Batch has been sent to event hub successfully!")
            createNewBatch()
        }
    }

    private fun isBatchEmpty(): Boolean {
        return batch.count == BigInteger.ZERO.toInt()
    }
}
