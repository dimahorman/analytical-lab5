package service

import com.fasterxml.jackson.databind.ObjectMapper
import data.Article
import data.DocType
import util.HtmlParserUtil
import java.util.*
import kotlin.collections.LinkedHashSet

class ParserService(private val baseUrl: String, private val path: String, private val mapper: ObjectMapper) {
    companion object {
        private const val PAGE_PATHS_SELECTOR = "ul.pagination li a"
        private const val TITLE_SELECTOR = "h4.text-right a"
        private const val DATE_SELECTOR = "div.col-sm-6"
        private const val ROW_SELECTOR = "div.nabor-inline"
    }

    fun parse(): List<String> {
        val pageSet = getPageSet()
        val articles = arrayListOf<String>()
        var i = 0

        pageSet.forEach { pagePath ->
            val pageUrl = baseUrl + pagePath
            val pageDoc = HtmlParserUtil.getConnection(pageUrl).get()
            val titleElements = pageDoc.select("$ROW_SELECTOR $TITLE_SELECTOR")
            val dateElements = pageDoc.select("$ROW_SELECTOR $DATE_SELECTOR")
                .filter {
                    it.text()[0].isDigit()
                }

            titleElements.zip(dateElements)
                .forEach { pair ->
                    i++
                    val date = pair.second
                        .text()
                        .split(" ")[0]

                    articles.add(
                        mapper.writeValueAsString(
                            Article(
                                pair.first.text(),
                                i,
                                getWordQuantity(),
                                getFormat(),
                                date
                            )
                        )
                    )
                }
        }

        return articles
    }

    fun getLinesQuantity(digitQuantity: Int): List<String> {
        val lines = LinkedList<String>()
        var i = digitQuantity

        while (i > 100) {
            val temp = i % 100
            if (temp != 0) {
                i -= temp
                lines.addFirst("$i-${i + temp}")
            } else {
                i -= 100
                lines.addFirst("${i}-${i + 100}")
            }
        }
        lines.addFirst("1-$i")
        return lines
    }

    private fun getPageSet(): LinkedHashSet<String> {
        val pageSet = linkedSetOf<String>()
        val doc = HtmlParserUtil.getConnection(baseUrl + path).get()
        doc.select(PAGE_PATHS_SELECTOR)
            .forEach { element ->
                pageSet.add(element.attr("href"))
            }
        val pageQuantity = pageSet
            .filter { it != "" }
            .maxOf {
                val strArray = it.split("=")
                strArray[1].toInt()
            }

        for (i in 1..pageQuantity) {
            pageSet.add("$path?page=$i")
        }
        return pageSet
    }

    private fun getFormat(): DocType {
        return when ((1..4).random()) {
            1 -> DocType.DOCX
            2 -> DocType.PDF
            3 -> DocType.XML
            4 -> DocType.XLSX
            else -> DocType.DOCX
        }
    }

    private fun getWordQuantity(): Int {
        return (50..80).random()
    }
}
