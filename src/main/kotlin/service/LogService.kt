package service

import redis.clients.jedis.Jedis

class LogService(private val redisClient: Jedis) {
    fun pushLinesParsedLog(linesQuantity: List<String>, fileName: String) {
        linesQuantity.forEach {
            val key = "$fileName-logger"
            val log = "Lines parsed - $it from $fileName"
            pushLog(key, log)
        }
    }

    fun pushLog(key: String, log: String) {
        redisClient.rpush(key, log)
    }

    fun printLogs(key: String) {
        for (i in 0..redisClient.llen(key)) {
            println(redisClient.lindex(key, i))
        }
    }

    fun setStatus(key: String, value: String) {
        redisClient.set(key, value)
    }

    fun getStatus(key: String): String? {
        return redisClient.get(key)
    }

    fun deleteStatus(key: String) {
        redisClient.del(key)
    }
}
